export type TPackageNameVersion = {

  name: string;

  version: string;

  url?: string;

};